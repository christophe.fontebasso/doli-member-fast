<?php
/**
 ***************************
 *    This file is part of DoliMemberFast .
 *
 *    DoliMemberFast is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    DoliMemberFast is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 **************************/

require('inc/functions.php');

/*
    Check authentication
*/
if( isset($_POST['token']) ) {
    require('inc/header.php');
    $token = $_POST['token'];

} else {
    
    // Try to get a token
    $response = api_login($_POST['login'], $_POST['password']);

    if($response->success) {
        require('inc/header.php');
    } else {
        header('Location: index.php?error=badloginpwd');
        exit();
    }

    $token = $response->success->token;
}



/*
    Make action on data if required
*/
?>

<pre>
<?php

if( isset($_POST['synccontacts']) ) {
    $members = api_members_without_contact($token);

    foreach($members as $member) {
        api_create_member_contact($member->id, $member->firstname, $member->lastname, $member->email, $member->phone_mobile, $token);
?>
   Contact créé pour Membre #<?=$member->id?> (<?=$member->firstname?> <?=$member->lastname?>)
<?php
    }
}
?>
</pre>


<?php
$memberId = NULL;
if( isset($_POST['id']) ) {
    // If an 'id' is posted, then the member is already created

    $memberId = $_POST['id'];

} else {
    if( isset($_POST['firstname']) ) {
        // If no 'id' is posted but a firstname, then we create the member

        $result = api_create_member($_POST['firstname'], $_POST['lastname'], $_POST['town'], $_POST['email'], $_POST['phone_mobile'], $token);
        echo "<div style=\"color:green;\">Nouveau membre \"".$_POST['firstname']." ".$_POST['lastname']."\" créé avec l'id <strong>$result</strong  ></div>";
        
        $memberId = $result;
    }

}

if( !is_null($memberId) && isset($_POST['subscription_date']) ) {
    // If a 'subscription_date' is posted, then we can create a subscription

    $result = create_subscription($memberId, $_POST['subscription_date'], $_POST['amount'], $_POST['note'], $token);
    echo "<div style=\"color:green;\">Nouvelle adhésion enregistrée avec la référence <strong>$result</strongs></div>";
}


?>
<form action="1_listmembers.php" method="post">
<header>
<h2>Dolibarr: ajout rapide d'adhérent</h2>
<p>1. le prénom  </p>
</header>
<label>Prénom</label>
<input name="firstname" placeholder="" type="text">
<input name="token" value="<?=$token?>" type="hidden">
<input id="submit" type="submit" value="Envoyer">
</form>

<pre>
<?php
$user = api_user_by_login($_POST['login'], $token);
if ($user[0]->admin == 1) {
?>

<form action="home.php" method="post">
<p>Synchronisation contacts  </p>
<input name="token" value="<?=$token?>" type="hidden">
<input name="synccontacts" value="1" type="hidden">
<input id="submit" type="submit" value="Synchroniser contacts">
</form>
<?php
}

?>
</pre>

<?php require('inc/footer.php'); ?>

