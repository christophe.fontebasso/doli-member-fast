<?php
/**
 ***************************
 *    This file is part of DoliMemberFast .
 *
 *    DoliMemberFast is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    DoliMemberFast is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 **************************/


// URL of the Dolibarr API
$DOL_API_URL_DIR = "https://DOMAIN_NAME/api/index.php/";


//Parameters to create a member
$DEFAULT_NEW_MEMBER_PARAMS = array(
    "morphy" => "phy",
    "public" => 0,
    "typeid" => "1",
    "type" => "Adhérent physique",
    "need_subscription" => "1",
    "entity" => "1",
    "statut" => "1");

//Parameters to create a subscription
$DEFAULT_NEW_SUBSCRIPTION_PARAMS = array(
    "subscription_interval_plus" => "P1Y", // Add a period of 1 year ...
    "subscription_interval_minus" => "P1D", // and... remove a period of 1 day, in order to have exactly a full year minus a day as duration for a subscription
    "note" => "Adhésion ".date("Y")
);

?>
