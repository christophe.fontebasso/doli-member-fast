<?php
/**
 ***************************
 *    This file is part of DoliMemberFast .
 *
 *    DoliMemberFast is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    DoliMemberFast is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 **************************/
require('config.php');




/** ***************************************************************************
    API - Login
*/
function api_login($login, $password) {
    global $DOL_API_URL_DIR;

    $apiCallOptions_array = array(
      'http'=>array(
        'method'=>"GET"
      )
    );

    $apiCallContext = stream_context_create($apiCallOptions_array);
    $url = $DOL_API_URL_DIR."login?login=".urlencode($login)."&password=".urlencode($password);
    $response = file_get_contents($url, false, $apiCallContext);
    return json_decode($response);
}




/** ***************************************************************************
    API - Retrieve a user by its login
*/
function api_user_by_login($login, $apiToken) {
    global $DOL_API_URL_DIR;

    $apiCallOptions_array = array(
      'http'=>array(
        'method'=>"GET",
        'header' => 'DOLAPIKEY: '.$apiToken
      )
    );

    $apiCallContext = stream_context_create($apiCallOptions_array);
    $url = $DOL_API_URL_DIR."users?sortfield=t.rowid&sortorder=ASC&limit=100&sqlfilters=t.login%3D'".urlencode($login)."'";

    try {
        $response = file_get_contents($url, false, $apiCallContext);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return json_decode($response);
}


/** ***************************************************************************
    API - Create a thirdparty
*/
function api_create_thirdparty($firstname, $lastname, $email, $phone_mobile, $apiToken) {
    global $DOL_API_URL_DIR;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);


    $dataArray = array(        
      "name" => "$firstname $lastname",
      "email" => $email,
      "phone" => $phone_mobile
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'thirdparties'.'?DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    
    if (curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);

    return $result;
}



/** ***************************************************************************
    API - List members by firstname
*/
function api_members_by_firstname($firstname, $apiToken) {
    global $DOL_API_URL_DIR;

    $apiCallOptions_array = array(
      'http'=>array(
        'method'=>"GET",
        'header' => 'DOLAPIKEY: '.$apiToken
      )
    );

    $apiCallContext = stream_context_create($apiCallOptions_array);
    $url = $DOL_API_URL_DIR."members?sortfield=t.firstname%2Ct.lastname&sortorder=ASC&limit=100&sqlfilters=(t.firstname%3Alike%3A'%25".urlencode($firstname)."%25')";

    try {
        $response = file_get_contents($url, false, $apiCallContext);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return json_decode($response);
}



/** ***************************************************************************
    API - List members without contact
*/
function api_members_without_contact($apiToken) {
    global $DOL_API_URL_DIR;

    $apiCallOptions_array = array(
      'http'=>array(
        'method'=>"GET",
        'header' => 'DOLAPIKEY: '.$apiToken
      )
    );

    $apiCallContext = stream_context_create($apiCallOptions_array);
//    $url = $DOL_API_URL_DIR."members?sortfield=t.rowid&sortorder=ASC&limit=500&sqlfilters=t.fk_soc%20is%20not%20null";
    $url = $DOL_API_URL_DIR."members?sortfield=t.rowid&sortorder=ASC&limit=100&sqlfilters=t.fk_soc%20is%20null";

    try {
        $response = file_get_contents($url, false, $apiCallContext);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return json_decode($response);
}



/** ***************************************************************************
    API - Retrieve a member by its id
*/
function api_member_by_id($id, $apiToken) {
    global $DOL_API_URL_DIR;

    $apiCallOptions_array = array(
      'http'=>array(
        'method'=>"GET",
        'header' => 'DOLAPIKEY: '.$apiToken
      )
    );

    $apiCallContext = stream_context_create($apiCallOptions_array);
    $url = $DOL_API_URL_DIR."members/".urlencode($id);

    try {
        $response = file_get_contents($url, false, $apiCallContext);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return json_decode($response);
}



/** ***************************************************************************
    API - Create a new member
*/
function api_create_member($firstname, $lastname, $town, $email, $phone_mobile, $apiToken) {
    global $DOL_API_URL_DIR;
    global $DEFAULT_NEW_MEMBER_PARAMS;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);


    $dataArray = array(        
      "town" => $town,
      "email" => $email,
      "phone_mobile" => $phone_mobile,
      "morphy" => $DEFAULT_NEW_MEMBER_PARAMS["morphy"],
      "public" => $DEFAULT_NEW_MEMBER_PARAMS["public"],
      "typeid" => $DEFAULT_NEW_MEMBER_PARAMS["typeid"],
      "type" => $DEFAULT_NEW_MEMBER_PARAMS["type"],
      "need_subscription" => $DEFAULT_NEW_MEMBER_PARAMS["need_subscription"],
      "entity" => $DEFAULT_NEW_MEMBER_PARAMS["entity"],
      "statut" => $DEFAULT_NEW_MEMBER_PARAMS["statut"],
      "lastname" => $lastname,
      "firstname" => $firstname
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'members'.'?DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    
    if (curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);

    return $result;
}




/** ***************************************************************************
    Create a new subscription (and update accordingly an existing member)
*/
function create_subscription($adherent_id, $subscription_date, $amount, $note, $apiToken) {
    global $DEFAULT_NEW_SUBSCRIPTION_PARAMS;

    // 0. manage subscription date
    $date = new DateTime($subscription_date); 
    $subscription_start_epochstamp = $date->format('U');
    $date->add(new DateInterval($DEFAULT_NEW_SUBSCRIPTION_PARAMS["subscription_interval_plus"])); 
    $date->sub(new DateInterval($DEFAULT_NEW_SUBSCRIPTION_PARAMS["subscription_interval_minus"])); 
    $subscription_end_epochstamp = $date->format('U');

    // 1. create the subscription object
    $subscriptionId = api_create_subscription($adherent_id, $subscription_start_epochstamp, $subscription_end_epochstamp, $amount, $note, $apiToken);

    // 2. get new subscription details
    $subscription = api_subscription_by_id($subscriptionId, $apiToken);

    // 3. update end of validity date
    $result = api_update_member_datefin($adherent_id, $subscription_end_epochstamp, $apiToken);

/* Useless but kept "just in case"
    // 3. get all data on the member
    $member = api_member_by_id($adherent_id, $apiToken);

    // 4a. if no subscription yet, update member "first" and "last" subscriptions data
    if (is_null($member->first_subscription_date) ) {
        api_update_member_first_subscription($adherent_id, $subscription->datec, $subscription_start_epochstamp, $subscription_end_epochstamp, $amount, $apiToken);
        api_update_member_last_subscription($adherent_id, $subscription->datec, $subscription_end_epochstamp, $subscription_end_epochstamp, $amount, $apiToken);
    } else {

        // 4b. if already a subscription, update only member "last" subscription data
        api_update_member_last_subscription($adherent_id, $subscription->datec, $subscription_start_epochstamp, $subscription_end_epochstamp, $amount, $apiToken);
    }
*/  
    
    return $subscriptionId;
}


/** ***************************************************************************
    API - Create a new subscription
*/
function api_create_subscription($adherent_id, $subscription_start_epochstamp, $subscription_end_epochstamp, $amount, $note, $apiToken) {
    global $DOL_API_URL_DIR;
    global $DEFAULT_NEW_SUBSCRIPTION_PARAMS;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);


    $dataArray = array(        
      "dateh" => $subscription_start_epochstamp,
      "datef" => $subscription_end_epochstamp,
      "fk_type" => "1",
      "fk_adherent" => $adherent_id,
      "amount" => $amount,
      "note" => $note
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'subscriptions'.'?DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
    
    if (curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);

    return $result;

}


/** ***************************************************************************
    API - Update a member thirdparty

*/
function api_update_member_thirdparty($adherent_id, $thirdparty_id, $apiToken) {
    global $DOL_API_URL_DIR;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $DOL_API_URL_DIR.'members/'.$adherent_id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"socid\": $thirdparty_id\n}\n");

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Accept: application/json';
    $headers[] = 'Dolapikey: '.$apiToken;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    return $result;
}



/** ***************************************************************************
    API - Update a member end of valid period information

*/
function api_update_member_datefin($adherent_id, $datefin_epochstamp, $apiToken) {
    global $DOL_API_URL_DIR;
    global $DEFAULT_NEW_MEMBER_PARAMS;

    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $DOL_API_URL_DIR.'members/'.$adherent_id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"datefin\": $datefin_epochstamp\n}\n");

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Accept: application/json';
    $headers[] = 'Dolapikey: '.$apiToken;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);


/*
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_PUT, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json',
                'DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);

    $dataArray = array(        
      "datefin" => $datefin_epochstamp
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    //curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'members'.'?id='.$adherent_id.'&DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'members'.'?id='.$adherent_id);    
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    
    if (!curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);
*/

    return $result;
}


/** ***************************************************************************
    API - Update a member first subscription information

*/
function api_update_member_first_subscription($adherent_id, $subscription_request_epochstamp, $subscription_start_epochstamp, $subscription_end_epochstamp, $amount, $apiToken) {
    global $DOL_API_URL_DIR;
    global $DEFAULT_NEW_MEMBER_PARAMS;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_PUT, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);

    $dataArray = array(        
      "first_subscription_date" => $subscription_request_epochstamp,
      "first_subscription_date_start" => $subscription_start_epochstamp,
      "first_subscription_date_end" => $subscription_end_epochstamp,
      "first_subscription_amount" => $amount
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'members'.'?id='.$adherent_id.'&DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    
    if (curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);

    return $result;
}


/** ***************************************************************************
    API - Update a member last subscription information

*/
function api_update_member_last_subscription($adherent_id, $subscription_request_epochstamp, $subscription_start_epochstamp, $subscription_end_epochstamp, $amount, $apiToken) {
    global $DOL_API_URL_DIR;
    global $DEFAULT_NEW_MEMBER_PARAMS;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_PUT, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);

    $dataArray = array(
      "datefin" => $subscription_end_epochstamp,        
      "last_subscription_date" => $subscription_request_epochstamp,
      "last_subscription_date_start" => $subscription_start_epochstamp,
      "last_subscription_date_end" => $subscription_end_epochstamp,
      "last_subscription_amount" => $amount
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'members'.'?id='.$adherent_id.'&DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    
    if (curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);

    return $result;
}


/** ***************************************************************************
    API - Retrieve a subscription by its id
*/
function api_subscription_by_id($id, $apiToken) {
    global $DOL_API_URL_DIR;

    $apiCallOptions_array = array(
      'http'=>array(
        'method'=>"GET",
        'header' => 'DOLAPIKEY: '.$apiToken
      )
    );

    $apiCallContext = stream_context_create($apiCallOptions_array);
    $url = $DOL_API_URL_DIR."subscriptions/".urlencode($id);

    try {
        $response = file_get_contents($url, false, $apiCallContext);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return json_decode($response);
}



/** ***************************************************************************
    API - Create a new contact
*/
function api_create_contact($thirdparty_id, $firstname, $lastname, $email, $phone_mobile, $apiToken) {
    global $DOL_API_URL_DIR;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);

    $httpheaders = array(
                'Content-Type: application/json',
                'Accept: application/json');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);

    $dataArray = array(        
      "firstname" => $firstname,
      "lastname" => $lastname,
      "socid" => $thirdparty_id,
      "phone_mobile" => $phone_mobile,
      "email" => $email
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataArray));

    curl_setopt($curl, CURLOPT_URL, $DOL_API_URL_DIR.'contacts'.'?DOLAPIKEY='.$apiToken);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    try {
        $result = curl_exec($curl);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
    
    if (curl_errno($curl)) {
      $info = curl_getinfo($curl);
      echo "<pre>";
        var_dump($info);
      echo "</pre>";

    }

    curl_close($curl);

    return $result;

}




/** ***************************************************************************
    API - Create a new contact
*/
function api_create_member_contact($adherent_id, $firstname, $lastname, $email, $phone_mobile, $apiToken) {
    $thirdparty_id = api_create_thirdparty($firstname, $lastname, $email, $phone_mobile, $apiToken);
    api_update_member_thirdparty($adherent_id, $thirdparty_id, $apiToken);
    api_create_contact($thirdparty_id, $firstname, $lastname, $email, $phone_mobile, $apiToken);
}


?>
